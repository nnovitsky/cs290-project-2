import React from 'react'
import './placedetails.css';


function PlaceDetail(props) {
  const curr = props.match.params.name;
  var num = 0;
  if (curr == "Maui"){
    num=0;
  }
  else if (curr == "London"){
    num=1;
  } 
  else {
    num=2; 
  }
  return (
    <div className='placeDetails'>
      <h1>{props.places[num].name}</h1>
      <p>Population: {props.places[num].pop}</p>
      <p>Country: {props.places[num].country}</p>
      <img src={props.places[num].img} width="600" />
    </div>
  )
}

export default PlaceDetail