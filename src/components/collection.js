import React from 'react';
import Place from './place';


function Collection(props){
    return (
        <div>
            {props.places.map(p => 
                <Place key={p.name} name={p.name} 
                country={p.country} pop={p.pop} img={p.img}/>
            )}
        </div>
    );
}

export default Collection