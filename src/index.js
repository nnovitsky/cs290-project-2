import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom'
import './index.css';
import App from './App';
import PlaceDetail from './components/placedetails'
import Notfound from './components/notfound'
import maui from './images/maui.jpg'
import london from './images/london.jpg'
import sydney from './images/sydney.jpg'

const places = [
  {name: "Maui", country: "USA", pop: "144,444", img: maui},
  {name: "London", country: "England", pop: "8.136 million", img: london},
  {name: "Sydney", country: "Australia", pop: "4.452 million", img: sydney}
];

const routing = (
    <Router>
      <div>
          <p>
                <Link to="/">Home</Link>
          </p>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/place/:name" render={(props) => <PlaceDetail {...props} places={places} />} />
          <Route component={Notfound} />
        </Switch>
      </div>
    </Router>
  )

ReactDOM.render(routing, document.getElementById('root'));
