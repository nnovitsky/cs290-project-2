import React from 'react';
import Collection from './components/collection';
import './App.css';
import maui from './images/maui.jpg'
import london from './images/london.jpg'
import sydney from './images/sydney.jpg'

const places = [
  {name: "Maui", country: "USA", pop: "144,444", img: maui},
  {name: "London", country: "England", pop: "8.136 million", img: london},
  {name: "Sydney", country: "Australia", pop: "4.452 million", img: sydney}
];

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Tanvi, Victoria, and Natalie's Favorite Places</h1>
        </header>

        <Collection places={places} />
      </div>
      
    );
  }
}
export default App